$( function() {
	var userName = window.location.hash.substr( 1 ); //Get the section after hash tag from URL, e.g. index.html#Arik return 'Arik'
	if ( userName === '' ) {
		console.log( "New user" );
	} else {
		$.get( '/user/' + userName, function( data ) { //This code makes an HTTP request to /arik and puts the data in the 'data' variable
			console.log( 'Got data', data ); //We just print whatever we got from the server {
			//Personal Details    
			$( '#firstName' ).val( data.basicInfo.firstName );
			$( '#lastName' ).val( data.basicInfo.lastName );
			$( '#JobTitle' ).val( data.basicInfo.jobTitle );
			$( '#phone' ).val( data.contactInfo.phone );
			$( '#address' ).val( data.contactInfo.address );
			$( '#email' ).val( data.contactInfo.email );
			$( '#aboutText' ).val( data.aboutMe.text );
			$( '#indexImg' ).val( data.aboutMe.image );
            
			/////Social Netwoks     
			//Making a loop that creates social network inputs, after creating the input field by using an existing function the loop looks for the links and puts it in the right field, i use j because i is used to create the fields and i is bigger htan j by 1
			for ( i in data.socialNetwork ) {
				if ( data.socialNetwork[ i ].link != null ) {
					var socialName = data.socialNetwork[ i ].name;
					addSocial( socialName );
					$( '#' + socialName + 'Link' ).val( data.socialNetwork[ i ].link );
				}
			}
            
			/////Experience     
			for ( i in data.experience ) {
				if ( i > 0 ) addMoreExp(); //if there is more than 1 experience we need to duplicate more fields in order to show all user data
				if ( data.experience[ i ].title != null ) {
					$( '#expTitle' + i + '' ).val( data.experience[ i ].title );
					$( '#expStartDate' + i + '' ).val( data.experience[ i ].startDate );
					$( '#expEndDate' + i + '' ).val( data.experience[ i ].endDate );
					$( '#expName' + i + '' ).val( data.experience[ i ].name );
					$( '#expDesc' + i + '' ).val( data.experience[ i ].description );
				}
			}
            
			/////Pro SKills     
			for ( i in data.proSkills ) {
				if ( i > 0 ) addProSkill();
				if ( data.proSkills[ i ].title != null ) {
					$( '#proSkill' + i + '' ).val( data.proSkills[ i ].title );
					$( '#proSkillPer' + i + '' ).val( data.proSkills[ i ].percent );
				}
			}
            
			/////Per SKills     
			for ( i in data.perSkills ) {
				if ( i > 0 ) addPerSkill();
				if ( data.perSkills[ i ].title != null ) {
					$( '#perSkill' + i + '' ).val( data.perSkills[ i ].title );
					$( '#perSkillPer' + i + '' ).val( data.perSkills[ i ].percent );
				}
			}
            
			/////Education     
			for ( i in data.education ) {
				if ( i > 0 ) addMoreEdu();
				if ( data.education[ i ].title != null ) {
					$( '#eduTitle' + i + '' ).val( data.education[ i ].title );
					$( '#eduStartDate' + i + '' ).val( data.education[ i ].startDate );
					$( '#eduEndDate' + i + '' ).val( data.education[ i ].endDate );
					$( '#eduPlace' + i + '' ).val( data.education[ i ].place );
					$( '#eduDesc' + i + '' ).val( data.education[ i ].description );
				}
			}
            
			/////Hobbies     
			for ( i in data.hobbies ) {
				if ( data.hobbies[ i ].name ) $( '#' + data.hobbies[ i ].name + '' ).prop( 'checked', 'true' );
			}
            
			/////Languages     
			for ( i in data.languages ) {
				if ( i > 0 ) addLang();
				if ( data.languages[ i ].name !== null ) {
					$( '#language' + i + '' ).val( data.languages[ i ].name );
					$( '#languagePer' + i + '' ).val( data.languages[ i ].percent );
				}
			}
		} );
	}
    
	/////Function that counts how many hobbies were selected,if the quantity is 5<g a message pops out yhat only 5 selections are enabled    
	$( 'input[type=checkbox]' ).on( 'change', function( e ) {
		if ( $( 'input[type=checkbox]:checked' ).length > 5 ) {
			$( this ).prop( 'checked', false );
			swal( "Opppsss...", "Max allowed - 5", "error" );
		}
	} );
	
	/////This section takes care of form submission - this code runs when user clicks the save button
	$( "form" ).submit( function( e ) {
		$.ajax( { //Send a request to the server with the form content
			type: "POST",
			url: "/user",
			data: $( "form" ).serialize(), // serializes the form's data
			success: function( data ) //This code runs when the request was successful
				{
					console.log( 'Success! CV Saved', data );
					swal( "Congrats!", "User added Successfully!", "success" );
					setTimeout( "window.location.replace('users.html')", 1000 );
				},
			error: function( error ) { //This code runs when there is a problem with sending the request
				console.error( 'Error in submitting form: ', error );
				swal( "ERROR!", "User has not been added!", "error" );
			}
		} );
		e.preventDefault(); // avoid to execute the actual submit of the form.
	} );
} );



////External functions
/////Social Networks-When the user clicks the social button this function adds input field that is named and related to the social netwirk that was chosen, the iSocial is used in order to save the data as array  
var iSocial = 0;
var count = 0; //counts the number of socialLnks that were chosen and limits it up to 5
function addSocial( soName ) {
	if ( count > 5 ) {
		swal( "Sorry..", "Tou can choose only six Social Networks", "error" );
	} else {
		$( "#soDiv" ).append( '<input type="text" id="' + soName + 'Link" placeholder="Enter your ' + soName + ' link" name="socialNetwork[' + iSocial + '][link]" required><input type="text" id="nameField' + soName + '" name="socialNetwork[' + iSocial + '][name]" value="' + soName + '" hidden="hidden"/><button type="button" id="close' + soName + '" class="closeBtn" onClick="remuveSocial(\'' + soName + '\')"><img src="img/close.png"></button>' );
		//soDiv.innerHTML = soDiv.innerHTML + ;
		iSocial++;
		count++;
		$( '#' + soName + 'Button' ).attr( 'disabled', true );
	}
}

/////Remuves social input field and decreases the iSocial in order to make the count right    
function remuveSocial( nameRemu ) {
	$( '#close' + nameRemu + '' ).remove();
	$( '#' + nameRemu + '' ).remove();
	$( '#name' + nameRemu + '' ).remove();
	$( '#' + nameRemu + 'Button' ).attr( 'disabled', false );
	count--;
}

/////Adds more Experiance section that contains full input in order to add additional Exp
var iExp = 1;

function addMoreExp() {
	$( "#expDiv" ).append( '<section id="exp' + iExp + '" class="expDes"><section><ul class="inputs"><li><input id="expTitle' + iExp + '" name="experience[' + iExp + '][title]" placeholder="Title" type="text" required></li><li><input id="expStartDate' + iExp + '" name="experience[' + iExp + '][startDate]" onblur="(this.type=\'text\')" onfocus="(this.type=\'date\')" placeholder="Start Date" type="text" required></li><li><input id="expEndDate' + iExp + '" name="experience[' + iExp + '][endDate]" onblur="(this.type=\'text\')" onfocus="(this.type=\'date\')" placeholder="End Date" type="text" required></li></ul></section><section><ul class="inputs"><li><input id="expName' + iExp + '" name="experience[' + iExp + '][name]" placeholder="Company Name" type="text" required></li><li><textarea id="expDesc' + iExp + '" cols="25" name="experience[' + iExp + '][description]" placeholder="Description" rows="4" required></textarea></li></ul></section><button type="button" id="closeExp" class="closeBtn" onClick="remuveExp(\'' + iExp + '\')"><img src="img/close.png"></button>' );
	iExp++;
}

function remuveExp( num ) {
	$( '#exp' + num + '' ).remove();
}

/////Adds more PerSkill input type    
var iPerSkill = 1;

function addPerSkill() {
	$( "#perSkillsDiv" ).append( '<ul class="inputs"><li><input id="perSkill' + iPerSkill + '" type="text" name="perSkills[' + iPerSkill + '][title]" placeholder="Per Title" required></li><li><input id="perSkillPer' + iPerSkill + '" type="number" max="100" min="1" name="perSkills[' + iPerSkill + '][percent]" placeholder="%" required></li><li><button type="button" id="closePerSkill' + iPerSkill + '" class="closeBtn" onClick="remuvePerSkill(\'' + iPerSkill + '\')"><img src="img/close.png"></button><li></ul>' );
	iPerSkill++;
}

function remuvePerSkill( num ) {
	$( '#perSkill' + num + '' ).parent().parent().remove();
}

/////Adds more ProSkill input type     
var iProSkill = 1;

function addProSkill() {
	$( "#proSkillsDiv" ).append( '<ul class="inputs"><li><input id="proSkill' + iProSkill + '" type="text" name="proSkills[' + iProSkill + '][title]" placeholder="Pro Title" required></li><li><input id="proSkillPer' + iProSkill + '" type="number" max="100" min="1" name="proSkills[' + iProSkill + '][percent]" placeholder="%" required></li><li><button type="button" id="closeProSkill' + iProSkill + '" class="closeBtn" onClick="remuveProSkill(\'' + iProSkill + '\')"><img src="img/close.png"></button><li></ul>' );
	iProSkill++;
}

function remuveProSkill( num ) {
	$( '#proSkill' + num + '' ).parent().parent().remove();
}

/////Adds more Education section that has input type in i
var iEdu = 1;

function addMoreEdu() {
	$( "#eduDiv" ).append( '<section id="edu' + iEdu + '" class="eduDes" style="margin-left: 24px;"><section><ul class="inputs"><li><input id="eduTitle' + iEdu + '" name="education[' + iEdu + '][title]" placeholder="Title" type="text" required></li><li><input id="eduStartDate' + iEdu + '" name="education[' + iEdu + '][startDate]" onblur="(this.type=\'text\')" onfocus="(this.type=\'date\')" placeholder="Start Date" type="text" required></li><li><input id="eduEndDate' + iEdu + '" name="education[' + iEdu + '][endDate]" onblur="(this.type=\'text\')" onfocus="(this.type=\'date\')" placeholder="End Date" type="text" required></li></ul></section><section><ul class="inputs"><li><input id="eduPlace' + iEdu + '" name="education[' + iEdu + '][place]" placeholder="Place" type="text" required></li><li><textarea id="eduDesc' + iEdu + '" cols="25" name="education[' + iEdu + '][description]" placeholder="Description" rows="4" required></textarea></li></ul></section><button type="button" id="closeEdu" class="closeBtn" onClick="remuveEdu(\'' + iEdu + '\')"><img src="img/close.png"></button></section>' );
	iEdu++;
}

function remuveEdu( num ) {
	$( '#edu' + num + '' ).remove();
}

/////Adds more Language input type
var iLang = 1;

function addLang() {
	$( "#languagesDiv" ).append( '<ul class="inputs"><li><input id="language' + iLang + '" type="text" pattern="[A-Za-z]{4,}" title="Must contain only letters!" name="languages[' + iLang + '][name]" placeholder="Title"></li><li><input id="languagePer' + iLang + '" type="number" max="100" min="1" name="languages[' + iLang + '][percent]" placeholder="%"></li><li><button type="button" id="closeLang" class="closeBtn" onClick="remuveLang(\'' + iLang + '\')"><img src="img/close.png"></button></li></ul>' );
	iLang++;
}

function remuveLang( num ) {
	$( '#language' + num + '' ).parent().parent().remove();
}