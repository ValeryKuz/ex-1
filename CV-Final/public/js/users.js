$(function() {
	///This section gets all the users from the server and lists them in client
	$.get('/users', function(users) {
		if (users == '') $('#usersTable').append('<h2>Sorry no users</h2>');
		else {
			$('#usersTable').append('<tr><th>Firstname</th><th>Lastname</th><th>E-mail</th><th></th><th></th><th></th</tr>');
			for ( var i in users) {
				$('#usersTable').append('<tr><td>' + users[i].basicInfo.firstName + '</td><td>' + users[i].basicInfo.lastName + '</td><td id="tdMail">' + users[i].contactInfo.email + '</td><td><button class="deleteBtn" value=' + users[i].basicInfo.firstName + '>Delete</button></td><td><a href="admin.html#' + users[i].basicInfo.firstName + '"><button class="userBtn">Edit</button></a></td><td><a href="index.html#' + users[i].basicInfo.firstName + '"><button class="userBtn">View</button></a></td></tr>');
				/// whenever i do onclick, the function knows what the button refers to..
			};
			///document is being used to overview the whole page after loaded and not before loading the data..*/
			$(document).on('click', "button.deleteBtn", function(e) {
				var firstName = $(e.target).val();
				$.get('/userdel/' + firstName, function() {
					$(e.target).parent().parent().remove(); //take the parent td and then it's parent, which is tr and remove it!!!
					swal("Deleted!", "Your user has been deleted.", "success");
					location.reload(); //if there was only one user and we deletet it the page is refreshed in order to make h2 appear instead of the table
				});
			});
		} //----else closure
	});
});