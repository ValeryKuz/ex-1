$( function() {
	var userName = window.location.hash.substr( 1 ); //Get the section after hash tag from URL, e.g. index.html#Arik return 'Arik'
	if ( userName === '' ) {
		swal( "No User selected!", "Please register or select your user :)" );
	} else {
		$.get( '/user/' + userName, function( data ) { //This code makes an HTTP request to /arik and puts the data in the 'data' variable
			console.log( 'Got data', data ); //We just print whatever we got from the server {
			if ( data === null ) swal( "No User Detected!", "Please register!", "warning" );
			
            /////Basic info
			$( '#intro header h1' ).text( data.basicInfo.firstName );
			$( '#intro header h2' ).text( data.basicInfo.lastName );
			$( '#intro header h3' ).text( data.basicInfo.jobTitle );
			
            /////Personal info 
			$( '#contact-info ul:nth-child(2) li:nth-child(1)' ).text( data.contactInfo.phone );
			$( '#contact-info ul:nth-child(2) li:nth-child(2)' ).text( data.contactInfo.address );
			$( '#contact-info ul:nth-child(2) li:nth-child(3)' ).remove(); ///removes last <li> where Loadingappears
			$( '#contact-info ul:nth-child(2)' ).append( '<li><a href="mailto:' + data.contactInfo.email + '">' + data.contactInfo.email + '</a></li>' ); ///inserts email link inside <li>
			
            /////About me
			$( '#about article' ).append( '<p>' + data.aboutMe.text + '</p>' );
			if ( data.aboutMe.image !== "" ) $( '#profileImgDiv' ).append( '<img id="profileImg" src="' + data.aboutMe.image + '"/>' );
			else $( '#profileImgDiv' ).append( '<img id="profileImg" src="img/profile.jpg"/>' );
			
            /////Social Netwoks     
			$ulSocial = $( '<ul>', {
				'id': 'social-networks'
			} );
			for ( var i in data.socialNetwork ) {
				var $templi = $( '<li>' );
				$templi.append( '<i class="fa fa-' + data.socialNetwork[ i ].name + '"></i>' );
				$templi.append( '<a href="' + data.socialNetwork[ i ].link + '">' + data.socialNetwork[ i ].name + '</a>' );
				$templi.click( function() {
					alert( $( this ).children( 'a' ).text() + ' clicked!' );
				} );
				//Add the new li element to the ul
				$ulSocial.append( $templi );
			}
			$ulSocial.insertAfter( '#intro' );
			
            /////Experience        
			$experienceAdd = $( '<section>', {
				'id': 'titles-info'
			} );
			for ( i in data.experience ) {
				var $tempSection = $( '<section id="ex-num" class="ex-info">' );
				$tempSection.append( '<section id="side-title"><h1 class="si-title">' + data.experience[ i ].title + '</h1><h2 class="ex-date">' + data.experience[ i ].startDate + '<br>' + data.experience[ i ].endDate + '</h2></section>' );
				$tempSection.append( '<section id="ex-description"><h1 class="small-description">' + data.experience[ i ].name + '</h1><p>' + data.experience[ i ].description + '</p></section>' );
				$experienceAdd.append( $tempSection );
			}
			$experienceAdd.appendTo( '#experiance' );
			
            /////Pro Skills
			$progAddSkills = $( '<div>', {
				'id': 'progBarSkills'
			} );
			for ( i in data.proSkills ) {
				if ( data.proSkills[ i ].title !== "" ) {
					var $tempProgSkills = $( '<section id="newProg">' );
					$tempProgSkills.append( '<section><h1>' + data.proSkills[ i ].title + '</h1></section>' );
					$tempProgSkills.append( '<section id="barDesing"><progress max="100" value="' + data.proSkills[ i ].percent + '"></progress></section>' );
					$progAddSkills.append( $tempProgSkills );
				}
			}
			$progAddSkills.appendTo( '#pro-skills' );
			
            /////Per Skills      
			$progAddPro = $( '<div>', {
				'id': 'progBarPer'
			} );
			for ( i in data.perSkills ) {
				if ( data.perSkills[ i ].title !== "" ) {
					var $tempProgSkills = $( '<section id="newProg">' );
					$tempProgSkills.append( '<section><h1>' + data.perSkills[ i ].title + '</h1></section>' );
					$tempProgSkills.append( '<section id="barDesing"><progress max="100" value="' + data.perSkills[ i ].percent + '"></progress></section>' );
					$progAddPro.append( $tempProgSkills );
				}
			}
			$progAddPro.appendTo( '#per-skills' );
			
            /////Eucation
			$educationAdd = $( '<section>', {
				'id': 'titles-info'
			} );
			for ( i in data.education ) {
				if ( data.education[ i ].title !== null ) {
					var $tempEducation = $( '<section id="ed-num" class="ed-info">' );
					$tempEducation.append( '<section id="ed-sideTitle"><h1 class="si-title">' + data.education[ i ].title + '</h1><h2 class="ed-date">' + data.education[ i ].startDate + '<br>' + data.education[ i ].endDate + '</h2></section>' );
					$tempEducation.append( '<section id="ed-description"><h1 class="small-description">' + data.education[ i ].place + '</h1><p>' + data.education[ i ].description + '</p></section>' );
					$educationAdd.append( $tempEducation );
				}
			}
			$educationAdd.appendTo( '#education' );
			
            /////Hobbies
			$hobbiesAdd = $( '<section>', {
				'id': 'hob-list'
			} );
			for ( i in data.hobbies ) {
				if ( data.hobbies[ i ].name ) {
					var $tempI = $( '<section class="hobName">' );
					$tempI.append( '<i class="fa fa-' + data.hobbies[ i ].icon + '"></i>' );
					$tempI.append( '<h1>' + data.hobbies[ i ].name + '</h1>' );
					$hobbiesAdd.append( $tempI );
				}
			}
			$hobbiesAdd.appendTo( '#hob-in' );
			
            /////Languages
			$addLanguages = $( '<div>', {
				'id': 'progBarLan'
			} );
			for ( i in data.languages ) {
				if ( data.languages[ i ].name !== "" ) {
					var $tempLang = $( '<section id="newProg">' );
					$tempLang.append( '<section><h1>' + data.languages[ i ].name + '</h1></section>' );
					$tempLang.append( '<section id="barDesing"><progress max="100" value="' + data.languages[ i ].percent + '"></progress></section>' );
					$addLanguages.append( $tempLang );
				}
			}
			$addLanguages.appendTo( '#languages' );
		} );
	}
} );