var express = require('express');

var app = express();

var port = 3000;
 
app.use(express.static('public'));

app.get('/val', function(req, res){
    res.json({
    	basicInfo: {
    		firstName:'Valerie', lastName:'Kuznetsov', title:'Fullstack developer'
    	},
        personalInfo: {
        phone:'0547451969', area:'Google,ElectraTower,TLV', eMail:'valery.k18@gmail.com'
        },
        article:{
           data: 'You�re a curious and collaborative visionary, someone excited about tackling the hard problems in technology. Google is and always will be an engineering company that thinks big and takes risks. Together, we�ll create and iterate on the products and tools of the future for billions of users.'
        },
        socialNetworks: [
	        {
	            name: 'Facebook',
	            link: '#',
	            'iconName': 'facebook'
	        },
	        {
	            name: 'Linkedin',
	            link: '#',
	            'iconName': 'linkedin'
	        },
	        {
	            name: 'Medium',
	            link: '#',
	            'iconName': 'medium'
	        },
	        {
	            name: 'Email',
	            link: '#',
	            'iconName': 'envelope'
	        },
	        {
	            name: 'Website',
	            link: '#',
	            'iconName': 'globe'
	        }
		],
        experiences:[
        {
            name: 'Senior Web Developer',
            date: 'Jul 2019 - Feb 2025',
            title: 'Facebook - TLV',
            contents: 'A web developer is a programmer who specializes in, or is specifically engaged in, the development of World Wide Web applications, or distributed network applications that are run over HTTP from a web server to a web browser.'
        },
        {
            name: 'Junior Web Developer',
            date: 'Jul 2017 - Feb 2019',
            title: 'We Work - New York',
            contents: 'A web developer is a programmer who specializes in, or is specifically engaged in, the development of World Wide Web applications, or distributed network applications that are run over HTTP from a web server to a web browser.'
        }
        ],
        progress:[
        {
            name: 'Photoshop',
            value:90
        },
        {
            name: 'Illustrator',
            value:75
        },
        {
            name: 'JavaScript',
            value:70
        },
        {
            name: 'Html/Css',
            value:80
        }
        ],    
        progressPro:[
        {
            name: 'Creativity',
            value:98
        },
        {
            name: 'Team work',
            value:87
        },
        {
            name: 'Hard work',
            value:75
        },
        {
            name: 'Leadership',
            value:90
        }
       ],      
       educations:[
        {
            name: 'SENIOR WEB DESIGNER',
            date: 'June 2016 - Sep 2019',
            title: 'Boston High School-California',
            contents: 'Web design encompasses many different skills and disciplines in the production and maintenance of websites.'
        },
        {
            name: 'SENIOR WEB DESIGNER',
            date: 'May 2011 - Apr 2016',
            title: 'Boston High School-California',
            contents: 'Web design encompasses many different skills and disciplines in the production and maintenance of websites.'
        }
       ],
       hobbies:[
        {
            name: 'Photography',
            'iconName': 'camera'
        },
        {
            name: 'Music',
            'iconName': 'headphones'
        },
        {
            name: 'Cooking',
           'iconName': 'cutlery'
        },
        {
            name: 'Drawing',
            'iconName': 'paint-brush'
        },
        {
            name: 'Reading',
            'iconName': 'book'
        },
        {
            name: 'Hiking',
            'iconName': 'leaf'
        }
      ],
     progressLang:[
        {
            name: 'Russian',
            value:100
        },
        {
            name: 'Hebrew',
            value:95
        },
        {
            name: 'English',
            value:85
        }
      ]
        
    });
});

app.listen(port, function(err){
    console.log('running server on port ' + port);
});