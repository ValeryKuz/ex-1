$(function() {
    $.get('/val', function(data) {
        
        console.log('Got data', data);
        
/////Basic info
        $('#intro header h1').text(data.basicInfo.firstName);
        $('#intro header h2').text(data.basicInfo.lastName);
        $('#intro header h3').text(data.basicInfo.title);
        
/////Personal info 
        $('#contact-info ul:nth-child(2) li:nth-child(1)').text(data.personalInfo.phone);
        $('#contact-info ul:nth-child(2) li:nth-child(2)').text(data.personalInfo.area);
        $('#contact-info ul:nth-child(2) li:nth-child(3)').text(data.personalInfo.eMail);
/////About me
  
        $(' #about article').append('<p>'+data.article.data+'</p>');
        
/////Social Netwoks     
        $ulSocial = $('<ul>', {
        'id': 'social-networks'
        });

    for(i in data.socialNetworks) {
        var $templi = $('<li>');
        
        $templi.append('<i class="fa fa-'+data.socialNetworks[i].iconName+'"></i>');
        $templi.append('<a href="'+data.socialNetworks[i].link+'">'+data.socialNetworks[i].name+'</a>');
        $templi.click(function(){
            alert($(this).children('a').text() + ' clicked!');
        });
        
        //Add the new li element to the ul
        $ulSocial.append($templi);
      }
    
       $ulSocial.insertAfter('#intro');
        
            
    
/////Experience Add
    $experienceAdd = $('<section>', {
        'id': 'titles-info'
    });    
    
        for(i in data.experiences) {
        var $tempSection = $('<section id="ex-num" class="ex-info">');
        
        $tempSection.append('<section id="side-title"><h1 class="pink-title">'+data.experiences[i].name+'</h1><h2 class="ex-date">'+data.experiences[i].date+'</h2></section>');
        $tempSection.append('<section id="ex-description"><h1 class="small-description">'+data.experiences[i].title+'</h1><p>'+data.experiences[i].contents+'</p></section>');
        $experienceAdd.append($tempSection);
      }

    $experienceAdd.appendTo('#experiance');
        
 /////Pro Skills
    $progAddSkills = $('<div>', {
        'id': 'progBarSkills'
    });
    
        for(i in data.progress) {
        var $tempProgSkills = $('<section id="newProg">');
            
        $tempProgSkills.append('<section><h1>'+data.progress[i].name+'</h1></section>');
        $tempProgSkills.append('<section id="barDesing"><progress max="100" value="'+data.progress[i].value+'"></progress></section>');
        $progAddSkills.append($tempProgSkills);
      }

    $progAddSkills.appendTo('#pro-skills');        
          
/////Per Skills      
    $progAddPro = $('<div>', {
        'id': 'progBarPer'
    });
    
        for(i in data.progressPro) {
        var $tempProgSkills = $('<section id="newProg">');
            
        $tempProgSkills.append('<section><h1>'+data.progressPro[i].name+'</h1></section>');
        $tempProgSkills.append('<section id="barDesing"><progress max="100" value="'+data.progressPro[i].value+'"></progress></section>');
        $progAddPro.append($tempProgSkills);
       }

    $progAddPro.appendTo('#per-skills');
        
/////Eucation
    $educationAdd = $('<section>', {
        'id': 'titles-info'
    });
    
        for(i in data.educations) {
        var $tempEducation = $('<section id="ed-num" class="ed-info">');
        
        $tempEducation.append('<section id="ed-sideTitle"><h1 class="pink-title">'+data.educations[i].name+'</h1><h2 class="ed-date">'+data.educations[i].date+'</h2></section>');
        $tempEducation.append('<section id="ed-description"><h1 class="small-description">'+data.educations[i].title+'</h1><p>'+data.educations[i].contents+'</p></section>');
        $educationAdd.append($tempEducation);
       }

    $educationAdd.appendTo('#education');
        
         
/////Hobbies
    $hobbiesAdd = $('<section>', {
        'id': 'hob-list'
    });

    for(i in data.hobbies) {
        var $tempI = $('<section class="hobName">');
        
        $tempI.append('<i class="fa fa-'+data.hobbies[i].iconName+'"></i>');
        $tempI.append('<h1>'+data.hobbies[i].name+'</h1>');   
        $hobbiesAdd.append($tempI);
     }
     
    $hobbiesAdd.appendTo('#hob-in');
    
    
    $addLanguages = $('<div>', {
        'id': 'progBarLan'
    });

        for(i in data.progressLang) {
        var $tempLang = $('<section id="newProg">');
            
        $tempLang.append('<section><h1>'+data.progressLang[i].name+'</h1></section>');
        $tempLang.append('<section id="barDesing"><progress max="100" value="'+data.progressLang[i].value+'"></progress></section>');
        $addLanguages.append($tempLang );
       }

      $addLanguages.appendTo('#languages');
        
});
    
});


