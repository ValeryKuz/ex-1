$(function(){
    
    var introHeader = {
        firstName: 'Valeria',
        lastName: 'Kuznetsov',
        title: 'Fullstack developer'
    };
    
    $('#intro header h1').text(introHeader.firstName);
    $('#intro header h2').text(introHeader.lastName);
    $('#intro header h3').text(introHeader.title);
    
    
    ////////////////
     var personalInfo = {
        phone:'0547451969',
        area: 'Google,ElectraTower,TLV',
        eMail:'valery.k18@gmail.com'
    };
    
    $('#contact-info ul:nth-child(2) li:nth-child(1)').text(personalInfo.phone);
    $('#contact-info ul:nth-child(2) li:nth-child(2)').text(personalInfo.area);
    $('#contact-info ul:nth-child(2) li:nth-child(3)').text(personalInfo.eMail);
    
    
    //Add p element inside article+txt
    $( "article" ).append( "<p>You’re a curious and collaborative visionary, someone excited about tackling the hard problems in technology. Google is and always will be an engineering company that thinks big and takes risks. Together, we’ll create and iterate on the products and tools of the future for billions of users.</p>" );
    
    
    /////////////////
    $ulSocial = $('<ul>', {
        'id': 'social-networks'
    });
    
    var socialNetworks = [
        {
            name: 'Facebook',
            link: '#',
            'iconName': 'facebook'
        },
        {
            name: 'Linkedin',
            link: '#',
            'iconName': 'linkedin'
        },
        {
            name: 'Medium',
            link: '#',
            'iconName': 'medium'
        },
        {
            name: 'Email',
            link: '#',
            'iconName': 'envelope'
        },
        {
            name: 'Website',
            link: '#',
            'iconName': 'globe'
        }
    ];

    for(i in socialNetworks) {
        var $templi = $('<li>');
        
        $templi.append('<i class="fa fa-'+socialNetworks[i].iconName+'"></i>');
        $templi.append('<a href="'+socialNetworks[i].link+'">'+socialNetworks[i].name+'</a>');
        
        $templi.click(function(){
            alert($(this).children('a').text() + ' clicked!');
        });
        
        //Add the new li element to the ul
        $ulSocial.append($templi);
    }
    
    $ulSocial.insertAfter('#intro');
    
    
    ///////////////////
    $experienceAdd = $('<section>', {
        'id': 'titles-info'
    });
    
    var experiences = [
        {
            name: 'Senior Web Developer',
            date: 'Jul 2019 - Feb 2025',
            title: 'Facebook - TLV',
            contents: 'A web developer is a programmer who specializes in, or is specifically engaged in, the development of World Wide Web applications, or distributed network applications that are run over HTTP from a web server to a web browser.'
        },
        {
            name: 'Junior Web Developer',
            date: 'Jul 2017 - Feb 2019',
            title: 'We Work - New York',
            contents: 'A web developer is a programmer who specializes in, or is specifically engaged in, the development of World Wide Web applications, or distributed network applications that are run over HTTP from a web server to a web browser.'
        }
    ];
    
        for(i in experiences) {
        var $tempSection = $('<section id="ex-num" class="ex-info">');
        
        $tempSection.append('<section id="side-title"><h1 class="pink-title">'+experiences[i].name+'</h1><h2 class="ex-date">'+experiences[i].date+'</h2></section>');
        $tempSection.append('<section id="ex-description"><h1 class="small-description">'+experiences[i].title+'</h1><p>'+experiences[i].contents+'</p></section>')
        
        $experienceAdd.append($tempSection);
    }

    $experienceAdd.appendTo('#experiance');
    
    
    /////////////
    $progAddSkills = $('<div>', {
        'id': 'progBarSkills'
    });
    
    var progress = [
        {
            name: 'Photoashop',
            value:90
        },
        {
            name: 'Illustrator',
            value:75
        },
        {
            name: 'JavaScript',
            value:70
        },
        {
            name: 'Html/Css',
            value:80
        }
    ];
    
        for(i in progress) {
        var $tempProgSkills = $('<section id="newProg">');
            
        $tempProgSkills.append('<section><h1>'+progress[i].name+'</h1></section>');
        $tempProgSkills.append('<section id="barDesing"><progress max="100" value="'+progress[i].value+'"></progress></section>');
        $( function() {
           $( "#progressbar" ).progressbar({
             value: progress[i].value
           });
        } );
    
        
        $progAddSkills.append($tempProgSkills);
    }

    $progAddSkills.appendTo('#pro-skills');
    
    
     ///////////////////
    $progAddPro = $('<div>', {
        'id': 'progBarPer'
    });
    
    var progressPro = [
        {
            name: 'Creativity',
            value:98
        },
        {
            name: 'Team work',
            value:87
        },
        {
            name: 'Hard work',
            value:75
        },
        {
            name: 'Leadership',
            value:90
        }
    ];
    
        for(i in progressPro) {
        var $tempProgSkills = $('<section id="newProg">');
            
        $tempProgSkills.append('<section><h1>'+progressPro[i].name+'</h1></section>');
        $tempProgSkills.append('<section id="barDesing"><progress max="100" value="'+progressPro[i].value+'"></progress></section>');
                $( function() {
    $( "#progressbar" ).progressbar({
      value:progressPro[i].value
    });
  } );
    
        
        $progAddPro.append($tempProgSkills);
    }

    $progAddPro.appendTo('#per-skills');
    
    /////////////
    $educationAdd = $('<section>', {
        'id': 'titles-info'
    });
    
    var educations = [
        {
            name: 'SENIOR WEB DESIGNER',
            date: 'June 2016 - Sep 2019',
            title: 'Boston High School-California',
            contents: 'Web design encompasses many different skills and disciplines in the production and maintenance of websites.'
        },
        {
            name: 'SENIOR WEB DESIGNER',
            date: 'May 2011 - Apr 2016',
            title: 'Boston High School-California',
            contents: 'Web design encompasses many different skills and disciplines in the production and maintenance of websites.'
        }
    ];
    
        for(i in educations) {
        var $tempEducation = $('<section id="ed-num" class="ed-info">');
        
        $tempEducation.append('<section id="ed-sideTitle"><h1 class="pink-title">'+educations[i].name+'</h1><h2 class="ed-date">'+educations[i].date+'</h2></section>');
        $tempEducation.append('<section id="ed-description"><h1 class="small-description">'+educations[i].title+'</h1><p>'+educations[i].contents+'</p></section>')
        
        $educationAdd.append($tempEducation);
    }

    $educationAdd.appendTo('#education');
    
    
    ///////////////////
    $hobbiesAdd = $('<section>', {
        'id': 'hob-list'
    });
    
    var hobbies = [
        {
            name: 'Photography',
            'iconName': 'camera'
        },
        {
            name: 'Music',
            'iconName': 'headphones'
        },
        {
            name: 'Cooking',
           'iconName': 'cutlery'
        },
        {
            name: 'Drawing',
            'iconName': 'paint-brush'
        },
        {
            name: 'Reading',
            'iconName': 'book'
        },
        {
            name: 'Hiking',
            'iconName': 'leaf'
        }
    ];

    for(i in hobbies) {
        var $tempI = $('<section class="hobName">');
        
        $tempI.append('<i class="fa fa-'+hobbies[i].iconName+'"></i>');
        $tempI.append('<h1>'+hobbies[i].name+'</h1>');   
        
        $hobbiesAdd.append($tempI);
    }
    
    $hobbiesAdd.appendTo('#hob-in');


    ////////////////
        $addLanguages = $('<div>', {
        'id': 'progBarSkills'
    });
    
    var progressLang = [
        {
            name: 'Russian',
            value:100
        },
        {
            name: 'Hebrew',
            value:95
        },
        {
            name: 'English',
            value:85
        }
    ];
    
        for(i in progressLang) {
        var $tempLang = $('<section id="newProg">');
            
        $tempLang.append('<section><h1>'+progressLang[i].name+'</h1></section>');
        $tempLang.append('<section id="barDesing"><progress max="100" value="'+progressLang[i].value+'"></progress></section>');
                $( function() {
    $( "#progressbar" ).progressbar({
      value: progressLang[i].value
    });
  } );
    
        
        $addLanguages.append($tempLang );
    }

    $addLanguages.appendTo('#languages');
    
    
       
});